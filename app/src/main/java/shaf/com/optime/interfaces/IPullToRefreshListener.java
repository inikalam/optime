package shaf.com.optime.interfaces;

public interface IPullToRefreshListener {
    void onRefresh();
}
