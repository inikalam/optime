package shaf.com.optime.interfaces;

public interface IDatePickerListener {
    void setDateResult(int year, int month, int day);
}
