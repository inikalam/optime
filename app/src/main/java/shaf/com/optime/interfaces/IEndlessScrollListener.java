package shaf.com.optime.interfaces;

public interface IEndlessScrollListener {
    void onLoad(int index, int totalCount);
}
