package shaf.com.optime.interfaces;

public interface INotifierListener {
    void onExecute();
}
