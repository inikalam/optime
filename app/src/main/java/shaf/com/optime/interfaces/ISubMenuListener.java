package shaf.com.optime.interfaces;

public interface ISubMenuListener {
    void onSelected();
}
