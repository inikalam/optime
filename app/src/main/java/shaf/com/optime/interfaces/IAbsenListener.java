package shaf.com.optime.interfaces;

import shaf.com.optime.models.Mentee;

public interface IAbsenListener {
    void absenAdded(Mentee mentee);
}
