package shaf.com.optime.interfaces;

public interface IConfirmationListener {
    void onOk();
    void onCancel();
}
