package shaf.com.optime.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import shaf.com.optime.R;

public class MateriViewHolder extends RecyclerView.ViewHolder {
    private TextView textTitle;
    private TextView textDeskripsi;
    private CircleImageView imageIcon;
    private ImageView downloadIcon;
    private LinearLayout layoutRoot;

    public MateriViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imageIcon = itemView.findViewById(R.id.image_icon);
        this.downloadIcon = itemView.findViewById(R.id.download_icon);
        this.textTitle = itemView.findViewById(R.id.text_title);
        this.textDeskripsi = itemView.findViewById(R.id.text_deskripsi);
        this.layoutRoot = itemView.findViewById(R.id.layout_root);
    }

    public TextView getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(TextView textTitle) {
        this.textTitle = textTitle;
    }

    public TextView getTextDeskripsi() {
        return textDeskripsi;
    }

    public void setTextDeskripsi(TextView textDeskripsi) {
        this.textDeskripsi = textDeskripsi;
    }

    public CircleImageView getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(CircleImageView imageIcon) {
        this.imageIcon = imageIcon;
    }

    public ImageView getDownloadIcon() {
        return downloadIcon;
    }

    public void setDownloadIcon(ImageView downloadIcon) {
        this.downloadIcon = downloadIcon;
    }

    public LinearLayout getLayoutRoot() {
        return layoutRoot;
    }

    public void setLayoutRoot(LinearLayout layoutRoot) {
        this.layoutRoot = layoutRoot;
    }
}
