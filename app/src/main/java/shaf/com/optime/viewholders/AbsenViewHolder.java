package shaf.com.optime.viewholders;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import shaf.com.optime.R;

public class AbsenViewHolder extends RecyclerView.ViewHolder {

    private RelativeLayout btnYes, btnNo;
    private TextView txtNama, txtKelas;

    public AbsenViewHolder(@NonNull View itemView) {
        super(itemView);
        this.btnYes = itemView.findViewById(R.id.btn_yes);
        this.btnNo = itemView.findViewById(R.id.btn_no);
        this.txtNama = itemView.findViewById(R.id.txt_nama);
        this.txtKelas = itemView.findViewById(R.id.txt_kelas);
    }

    public RelativeLayout getBtnYes() {
        return btnYes;
    }

    public void setBtnYes(RelativeLayout btnYes) {
        this.btnYes = btnYes;
    }

    public RelativeLayout getBtnNo() {
        return btnNo;
    }

    public void setBtnNo(RelativeLayout btnNo) {
        this.btnNo = btnNo;
    }

    public TextView getTxtNama() {
        return txtNama;
    }

    public void setTxtNama(TextView txtNama) {
        this.txtNama = txtNama;
    }

    public TextView getTxtKelas() {
        return txtKelas;
    }

    public void setTxtKelas(TextView txtKelas) {
        this.txtKelas = txtKelas;
    }
}
