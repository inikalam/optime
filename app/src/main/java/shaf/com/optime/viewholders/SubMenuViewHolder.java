package shaf.com.optime.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import shaf.com.optime.R;

public class SubMenuViewHolder extends RecyclerView.ViewHolder{
    private ImageView imageThumbnail;
    private TextView textTitle;
    private LinearLayout layoutRoot;

    public ImageView getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(ImageView imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public TextView getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(TextView textTitle) {
        this.textTitle = textTitle;
    }

    public LinearLayout getLayoutRoot() {
        return layoutRoot;
    }

    public void setLayoutRoot(LinearLayout layoutRoot) {
        this.layoutRoot = layoutRoot;
    }

    public SubMenuViewHolder(@NonNull View itemView) {
        super(itemView);
        textTitle = itemView.findViewById(R.id.text_title);
        imageThumbnail = itemView.findViewById(R.id.thumbnail);
        layoutRoot = itemView.findViewById(R.id.master_root_layout);
    }
}
