package shaf.com.optime.models;

import java.io.Serializable;

public class Kelompok implements Serializable {
    private String jumlahmentee;
    private String mentor;
    private String nama;
    private String tingkat;
    private String key;

    public Kelompok(String jumlahmentee, String mentor, String nama, String tingkat) {
        this.jumlahmentee = jumlahmentee;
        this.mentor = mentor;
        this.nama = nama;
        this.tingkat = tingkat;
    }

    public String getJumlahmentee() {
        return jumlahmentee;
    }

    public void setJumlahmentee(String jumlahmentee) {
        this.jumlahmentee = jumlahmentee;
    }

    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTingkat() {
        return tingkat;
    }

    public void setTingkat(String tingkat) {
        this.tingkat = tingkat;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
