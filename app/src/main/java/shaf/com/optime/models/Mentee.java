package shaf.com.optime.models;

import java.io.Serializable;

public class Mentee implements Serializable {
    private String angkatan;
    private String gender;
    private String jurusan;
    private String kelompok;
    private String mentor;
    private String nama;
    private String telepon;
    private String alamat;
    private String hobi;
    private String goldar;
    private String catatan;
    private String key;

    public Mentee(String angkatan, String gender, String jurusan, String kelompok, String mentor, String nama, String telepon, String alamat, String hobi, String goldar, String catatan) {
        this.angkatan = angkatan;
        this.gender = gender;
        this.jurusan = jurusan;
        this.kelompok = kelompok;
        this.mentor = mentor;
        this.nama = nama;
        this.telepon = telepon;
        this.alamat = alamat;
        this.hobi = hobi;
        this.goldar = goldar;
        this.catatan = catatan;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getHobi() {
        return hobi;
    }

    public void setHobi(String hobi) {
        this.hobi = hobi;
    }

    public String getGoldar() {
        return goldar;
    }

    public void setGoldar(String goldar) {
        this.goldar = goldar;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getAngkatan() {
        return angkatan;
    }

    public void setAngkatan(String angkatan) {
        this.angkatan = angkatan;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getKelompok() {
        return kelompok;
    }

    public void setKelompok(String kelompok) {
        this.kelompok = kelompok;
    }

    public String getMentor() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor = mentor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
