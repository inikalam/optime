package shaf.com.optime.models;

public class Laporan {
    private String kelompok;
    private String materi;
//    private String topik;
    private String tempat;
    private String tanggal;
    private String waktu;
    private String jumlahHadir;
    private String jumlahTidakHadir;
    private String keteranganKehadiran;

    public String getKelompok() {
        return kelompok;
    }

    public void setKelompok(String kelompok) {
        this.kelompok = kelompok;
    }

    public String getMateri() {
        return materi;
    }

    public void setMateri(String materi) {
        this.materi = materi;
    }

    //    public String getTopik() {
//        return topik;
//    }
//
//    public void setTopik(String topik) {
//        this.topik = topik;
//    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJumlahHadir() {
        return jumlahHadir;
    }

    public void setJumlahHadir(String jumlahHadir) {
        this.jumlahHadir = jumlahHadir;
    }

    public String getJumlahTidakHadir() {
        return jumlahTidakHadir;
    }

    public void setJumlahTidakHadir(String jumlahTidakHadir) {
        this.jumlahTidakHadir = jumlahTidakHadir;
    }

    public String getKeteranganKehadiran() {
        return keteranganKehadiran;
    }

    public void setKeteranganKehadiran(String keteranganKehadiran) {
        this.keteranganKehadiran = keteranganKehadiran;
    }
}
