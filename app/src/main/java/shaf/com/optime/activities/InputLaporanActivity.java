package shaf.com.optime.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import shaf.com.optime.R;
import shaf.com.optime.adapters.AbsenMenteeAdapter;
import shaf.com.optime.interfaces.IAbsenListener;
import shaf.com.optime.models.Laporan;
import shaf.com.optime.models.Mentee;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;
import shaf.com.optime.utilities.ListProvider;

public class InputLaporanActivity extends MasterActivity {

    private Toolbar toolbar;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private BetterSpinner inputMateri;
    private Spinner inputKelompok;
    private EditText etMateri, etTempat, etWaktu, etTanggal;
    private RecyclerView recyclerView;
    private Button buttonSubmit;
    private AbsenMenteeAdapter absenMenteeAdapter;
    private List<Mentee> mentees;
    private String keteranganKehadiran = "";
    private StringBuilder sb = new StringBuilder();
    private Integer jumlahHadir = 0, jumlahTidakHadir = 0;

    String[] SPINNERLIST = {"Izin", "Sakit", "Lainnya"};

    FirebaseAuth auth;
    String userLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_laporan);

        toolbar = findViewById(R.id.toolbar);
        inputKelompok = findViewById(R.id.spinner_kelompok);
        etMateri = findViewById(R.id.et_materi);
//        inputMateri = findViewById(R.id.spinner_topik);
        etTempat = findViewById(R.id.et_tempat);
        etTanggal = findViewById(R.id.et_tanggal);
        etWaktu = findViewById(R.id.et_waktu);
        buttonSubmit = findViewById(R.id.btn_submitLaporan);
        recyclerView = findViewById(R.id.recycler_view_kehadiran);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Input Laporan");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showProgressBarDialog();

        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        etTanggal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(InputLaporanActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etWaktu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(InputLaporanActivity.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        etWaktu.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        getDataSpinner();

        inputKelompok.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                final String[] separated = inputKelompok.getSelectedItem().toString().split(",");
                separated[0] = separated[0].trim();
                separated[1] = separated[1].trim();
                Log.e("Udah pilih kelompok", "Terpilih");
                loadData(separated[0]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBarDialog();
                final String[] separated = inputKelompok.getSelectedItem().toString().split(",");
                separated[0] = separated[0].trim();

                addLaporanToDatabase(separated[0], etMateri.getText().toString(), etTempat.getText().toString(), etTanggal.getText().toString(), etWaktu.getText().toString(), jumlahHadir.toString(), jumlahTidakHadir.toString(), keteranganKehadiran);
            }
        });

        mentees = new ArrayList<>();
        absenMenteeAdapter = new AbsenMenteeAdapter(mentees, this);
        ListProvider.begin(recyclerView, absenMenteeAdapter, 1);
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        etTanggal.setText(sdf.format(myCalendar.getTime()));
    }

    private void getDataSpinner() {
        auth = FirebaseAuth.getInstance();
        Log.e("USER LOGIN", "onCreate: " + auth.getCurrentUser().getEmail());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference();

        ref.child("Mentor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot mentorSnapshot : dataSnapshot.getChildren()) {
                    String namaMentor = mentorSnapshot.child("fullname").getValue(String.class);
                    String emailMentor = mentorSnapshot.child("email").getValue(String.class);
                    if (emailMentor.equals(auth.getCurrentUser().getEmail())) {
                        userLogin = namaMentor;
                        Log.e("USER LOGIN", "getdatadong: " + userLogin);

                        ref.child("Kelompok").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                final List<String> kelompoks = new ArrayList<>();

                                for (DataSnapshot kelompokSnapshot : dataSnapshot.getChildren()) {
                                    String namaKelompok = kelompokSnapshot.child("nama").getValue(String.class);
                                    String namaMentor = kelompokSnapshot.child("mentor").getValue(String.class);
                                    if (namaMentor.equals(userLogin) && namaKelompok != null) {
                                        kelompoks.add(namaKelompok + " , " + namaMentor);
                                        Log.e("USER LOGIN", "namanya: " + userLogin);
                                        Log.e("Kelompok", "onDataChange: " + kelompoks);
                                        if (kelompoks.isEmpty()) {
                                            finish();
                                            PopupManager.showToast("Anda belum memiliki kelompok binaan");
                                        }
                                    }
                                }

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(InputLaporanActivity.this,
                                        android.R.layout.simple_dropdown_item_1line, kelompoks);
                                inputKelompok.setAdapter(adapter);
                                dismissProgressBarDialog();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        ref.child("Materi").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                final List<String> materis = new ArrayList<>();
//
//                for (DataSnapshot materiSnapshot : dataSnapshot.getChildren()) {
//                    String namaMateri = materiSnapshot.child("nama").getValue(String.class);
//                    if (namaMateri != null) {
//                        materis.add(namaMateri);
//                    }
//                }
//
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(InputLaporanActivity.this,
//                        android.R.layout.simple_dropdown_item_1line, materis);
//                inputMateri.setAdapter(adapter);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
    }

    void loadData(String kelompok) {
        mentees.clear();
        ABSEN.initAbsen(kelompok, new IAbsenListener() {
            @Override
            public void absenAdded(Mentee mentee) {
                try {
                    mentees.add(mentee);
                    absenMenteeAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                }
                Log.e("Mentee Banyak", "absenAdded size: " + mentees.size());
            }
        });
    }

    public void setStatus(final String namaMentee, final boolean status) {
        if (status) {
            jumlahHadir += 1;
            sb.append(namaMentee + " - Hadir").append('\n');
            keteranganKehadiran = sb.toString();
            Log.e("Hadir", "setStatus: " + jumlahHadir.toString() + " " + keteranganKehadiran);
        } else {
            final android.app.AlertDialog dialogBuilder = new android.app.AlertDialog.Builder(InputLaporanActivity.this).create();
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_alasan, null);

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, SPINNERLIST);

            final BetterSpinner materialDesignSpinner = dialogView.findViewById(R.id.android_material_design_spinner);
            materialDesignSpinner.setAdapter(arrayAdapter);
            Button btnSubmit = dialogView.findViewById(R.id.btn_submit);
            Button btnCancel = dialogView.findViewById(R.id.btn_cancel);

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!materialDesignSpinner.getText().toString().equals("")) {
                        jumlahTidakHadir += 1;
                        sb.append(namaMentee + " - Tidak Hadir, " + materialDesignSpinner.getText().toString()).append('\n');
                        keteranganKehadiran = sb.toString();
                        Log.e("Tidak Hadir", "setStatus: " + jumlahTidakHadir.toString() + " " + keteranganKehadiran);
                        dialogBuilder.dismiss();
                    } else {
                        PopupManager.show("Maaf", "Anda Harus Mengisi Alasan");
                    }
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogBuilder.dismiss();
                }
            });

            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
            dialogBuilder.show();

        }
    }

    private void addLaporanToDatabase(String kelompok, String materi, String tempat, String tanggal, String waktu, String jumlahHadir, String jumlahTidakHadir, String keteranganKehadiran) {

        Laporan laporan = new Laporan();

        laporan.setKelompok(kelompok);
        laporan.setMateri(materi);
//        laporan.setTopik(topik);
        laporan.setTempat(tempat);
        laporan.setTanggal(tanggal);
        laporan.setWaktu(waktu);
        laporan.setJumlahHadir(jumlahHadir);
        laporan.setJumlahTidakHadir(jumlahTidakHadir);
        laporan.setKeteranganKehadiran(keteranganKehadiran);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("Laporan");
        ref.push().setValue(laporan, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    PopupManager.showToast("Gagal menambahkan laporan");
                    dismissProgressBarDialog();
                } else {
                    PopupManager.showToast("Berhasil menambahkan laporan");
                    dismissProgressBarDialog();
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
