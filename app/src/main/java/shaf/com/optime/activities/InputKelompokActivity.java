package shaf.com.optime.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.models.Kelompok;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;

public class InputKelompokActivity extends MasterActivity {

    Toolbar toolbar;
    EditText input_namaKelompok, input_tingkatMentee, input_jumlahMentee;
    BetterSpinner input_namaMentor;
    Button submitKelompok;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference("Kelompok");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kelompok);

        toolbar = findViewById(R.id.toolbar);
        input_namaKelompok = findViewById(R.id.et_namaKelompok);
        input_tingkatMentee = findViewById(R.id.et_tingkatMentee);
        input_jumlahMentee = findViewById(R.id.et_jumlahMentee);
        input_namaMentor = findViewById(R.id.spinner_mentor);
        submitKelompok = findViewById(R.id.btn_submitKelompok);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Input Kelompok");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showProgressBarDialog();
        getMentorSpinner();

        final Kelompok kelompok = (Kelompok) getIntent().getSerializableExtra("data");
        if (kelompok != null){
            input_namaKelompok.setText(kelompok.getNama());
            input_tingkatMentee.setText(kelompok.getTingkat());
            input_jumlahMentee.setText(kelompok.getJumlahmentee());
            input_namaMentor.setText(kelompok.getMentor());
            submitKelompok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProgressBarDialog();
                    kelompok.setNama(input_namaKelompok.getText().toString());
                    kelompok.setTingkat(input_tingkatMentee.getText().toString());
                    kelompok.setJumlahmentee(input_jumlahMentee.getText().toString());
                    kelompok.setMentor(input_namaMentor.getText().toString());
                    updateKelompok(kelompok);
                }
            });
        } else {
            submitKelompok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProgressBarDialog();
                    if (!isEmpty(input_namaMentor.getText().toString()) && !isEmpty(input_namaKelompok.getText().toString()) && !isEmpty(input_tingkatMentee.getText().toString()) && !isEmpty(input_jumlahMentee.getText().toString())){
                        addKelompok(new Kelompok(input_jumlahMentee.getText().toString(), input_namaMentor.getText().toString(), input_namaKelompok.getText().toString(), input_tingkatMentee.getText().toString().toUpperCase()));
                    } else {
                        PopupManager.show("Peringatan", "Data tidak boleh kosong");
                    }
                }
            });
        }
    }

    private boolean isEmpty(String s){
        return TextUtils.isEmpty(s);
    }

    private void getMentorSpinner(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ref.child("Mentor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> mentors = new ArrayList<>();

                for (DataSnapshot mentorSnapshot: dataSnapshot.getChildren()) {
                    String namaMentor = mentorSnapshot.child("fullname").getValue(String.class);
                    if (namaMentor!=null){
                        mentors.add(namaMentor);
                        Log.e("Mentors", "onDataChange: " + mentors );
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(InputKelompokActivity.this,
                        android.R.layout.simple_dropdown_item_1line, mentors);
                input_namaMentor.setAdapter(adapter);
                dismissProgressBarDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateKelompok(Kelompok kelompok){
        ref.child(kelompok.getKey()).setValue(kelompok).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                PopupManager.showToast("Data Berhasil Diupdate");
                finish();
                NavigationManager.openPage(DataKelompokActivity.class);
            }
        });
    }

    private void addKelompok(Kelompok kelompok){
        ref.push().setValue(kelompok, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    PopupManager.showToast("Gagal menambahkan kelompok");
                    dismissProgressBarDialog();
                } else {
                    PopupManager.showToast("Berhasil menambahkan kelompok");
                    dismissProgressBarDialog();
                    input_namaMentor.setText("");
                    input_jumlahMentee.setText("");
                    input_tingkatMentee.setText("");
                    input_namaKelompok.setText("");
                    input_namaKelompok.clearFocus();
                    input_jumlahMentee.clearFocus();
                    input_tingkatMentee.clearFocus();
                }
            }
        });
    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, InputKelompokActivity.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
