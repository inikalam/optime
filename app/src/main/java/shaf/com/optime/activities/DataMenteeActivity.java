package shaf.com.optime.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.adapters.DataMenteeAdapter;
import shaf.com.optime.models.Mentee;
import shaf.com.optime.services.FormatManager;
import shaf.com.optime.utilities.ListProvider;

public class DataMenteeActivity extends MasterActivity {

    FirebaseAuth auth;
    String userLogin, namaKelompok;
    private RecyclerView recyclerView;
    private DataMenteeAdapter adapter;
    private List<Mentee> mentees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_mentee);

        recyclerView = findViewById(R.id.recycler_view);

        getData();
        mentees = new ArrayList<>();
        adapter = new DataMenteeAdapter(mentees, this);
        ListProvider.begin(recyclerView, adapter, 1);
    }

    public String getNamaKelompok() {
        return namaKelompok;
    }

    public void afterUpdate(){
        adapter.notifyDataSetChanged();
    }

    private void getData(){
        Bundle extras = getIntent().getExtras();
        namaKelompok = extras.getString("namaKelompok");
        Log.e("TAG", "namaKelompok: " + namaKelompok );

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference();

        ref.child("Mentee").child(namaKelompok).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e("TAG", "onDataChange: Masuk ga kesini" + dataSnapshot );
                Log.e("TAG", "onDataChange: Masuk ga kesini" + dataSnapshot.getChildren() );
                Log.e("TAG", "onDataChange: Masuk ga kesini" + dataSnapshot.toString() );
                for (DataSnapshot menteeSnapshot : dataSnapshot.getChildren()){
                    String namaMentor = menteeSnapshot.child("nama").getValue(String.class);
                    Log.e("TAG", "onDataChange: " + namaMentor);
                    Mentee mentee = FormatManager.parseTo(Mentee.class, new Gson().toJson(menteeSnapshot.getValue()));
                    mentee.setKey(menteeSnapshot.getKey());
                    Log.e("TAG", "onDataChange: " + mentee );
                    Log.e("TAG", "onDataChange: " + mentee.toString() );
                    Log.e("TAG", "onDataChange: " + mentee.getNama() );
                    Log.e("TAG", "onDataChange: " + new Gson().toJson(menteeSnapshot.getValue()) );
                    if (mentee != null){
                        mentees.add(mentee);
                        adapter.notifyDataSetChanged();
                    }
                }
                dismissProgressBarDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
