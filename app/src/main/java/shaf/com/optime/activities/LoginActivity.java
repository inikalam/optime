package shaf.com.optime.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import shaf.com.optime.R;
import shaf.com.optime.services.DisplayManager;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;

public class LoginActivity extends MasterActivity {
    Button btnLogin;
    TextView textRegister;

    FirebaseAuth auth;

    EditText inputEmail, inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.button_signin);
        textRegister = findViewById(R.id.text_register);
        inputEmail = findViewById(R.id.text_email);
        inputPassword = findViewById(R.id.text_password);

        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null){
            NavigationManager.openPage(MainActivity.class);
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBarDialog();
                String email = inputEmail.getText().toString();
                final String password = inputPassword.getText().toString();

                if (TextUtils.isEmpty(email)){
                    PopupManager.showToast("Masukkan email terlebih dahulu");
                    return;
                }
                if (TextUtils.isEmpty(password)){
                    PopupManager.showToast("Masukkan kata sandi terlebih dahulu");
                    return;
                }

                auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()){
                            if (password.length()<6){
                                inputPassword.setError(getString(R.string.alert_password_minimum));
                                dismissProgressBarDialog();
                            } else {
                                PopupManager.showToast("Login Gagal " + task.getException());
                                dismissProgressBarDialog();
                            }
                        } else {
                            dismissProgressBarDialog();
                            NavigationManager.openPage(MainActivity.class);
                            finish();
                        }
                    }
                });
            }
        });

        textRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationManager.openPage(RegisterActivity.class);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}