package shaf.com.optime.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.FirebaseDatabase;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import shaf.com.optime.R;
import shaf.com.optime.models.Mentor;
import shaf.com.optime.services.DisplayManager;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;

public class RegisterActivity extends MasterActivity {

    EditText inputName;
    EditText inputEmail;
    EditText inputPhone;
    EditText inputAddress;
    EditText inputAngkatan;
    EditText inputJurusan;
    EditText inputPassword;

    BetterSpinner inputGender;

    Button buttonSubmit;
    TextView textAppVersion;
    TextView textLogin;

    private ProgressBar progressBar;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayManager.setFullScreen();
        setContentView(R.layout.activity_register);
        initLoader();

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        inputName = findViewById(R.id.text_name);
        inputEmail = findViewById(R.id.text_email);
        inputPhone = findViewById(R.id.text_phone);
        inputAddress = findViewById(R.id.text_alamat);
        inputAngkatan = findViewById(R.id.text_angkatan);
        inputJurusan = findViewById(R.id.text_jurusan);
        inputPassword = findViewById(R.id.text_password);

        inputGender = findViewById(R.id.spinner_gender);

        buttonSubmit = findViewById(R.id.button_submit);
        textLogin = findViewById(R.id.text_login);

        String[] list = {"Ikhwan", "Akhwat"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        inputGender.setAdapter(adapter);

        textLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationManager.openPage(LoginActivity.class);
                finish();
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBarDialog();
                final String nama = inputName.getText().toString();
                final String email = inputEmail.getText().toString();
                final String phone = inputPhone.getText().toString();
                final String alamat = inputAddress.getText().toString();
                final String angkatan = inputAngkatan.getText().toString();
                final String jurusan = inputJurusan.getText().toString();
                final String password = inputPassword.getText().toString();
                final String gender = inputGender.getText().toString();

                auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<com.google.firebase.auth.AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<com.google.firebase.auth.AuthResult> task) {
                        if (!task.isSuccessful()) {
                            PopupManager.showToast("Authentication failed." + task.getException());
                        } else {
                            addUserTodatabase(email, password, phone, nama, alamat, angkatan, jurusan, gender);
                            dismissProgressBarDialog();
                            NavigationManager.openPage(LoginActivity.class);
                            finish();
                        }
                    }
                });
            }
        });
    }

    private void addUserTodatabase(String email, String password, String phone, String nama, String alamat, String angkatan, String jurusan, String gender){

        Mentor mentor = new Mentor();

        mentor.setEmail(email);
        mentor.setPassword(password);
        mentor.setFullname(nama);
        mentor.setAlamat(alamat);
        mentor.setTelepon(phone);
        mentor.setAngkatan(angkatan);
        mentor.setJurusan(jurusan);
        mentor.setGender(gender);

        FirebaseDatabase.getInstance().getReference("Mentor").push().setValue(mentor);
    }

}
