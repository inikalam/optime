package shaf.com.optime.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.adapters.DataKelompokAdapter;
import shaf.com.optime.models.Kelompok;
import shaf.com.optime.services.FormatManager;
import shaf.com.optime.services.PopupManager;
import shaf.com.optime.utilities.ListProvider;

public class DataKelompokActivity extends MasterActivity {

    FirebaseAuth auth;
    String userLogin;
    private RecyclerView recyclerView;
    private DataKelompokAdapter adapter;
    private List<Kelompok> kelompoks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_kelompok);

        recyclerView = findViewById(R.id.recycler_view);

        getData();
        kelompoks = new ArrayList<>();
        adapter = new DataKelompokAdapter(kelompoks, this);
        ListProvider.begin(recyclerView, adapter, 1);
    }

    public void afterUpdate(){
        adapter.notifyDataSetChanged();
    }

    private void getData() {
        auth = FirebaseAuth.getInstance();
        Log.e("USER LOGIN", "onCreate: " + auth.getCurrentUser().getEmail());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference();

        ref.child("Mentor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot mentorSnapshot : dataSnapshot.getChildren()) {
                    String namaMentor = mentorSnapshot.child("fullname").getValue(String.class);
                    String emailMentor = mentorSnapshot.child("email").getValue(String.class);
                    if (emailMentor.equals(auth.getCurrentUser().getEmail())) {
                        userLogin = namaMentor;
                        Log.e("USER LOGIN", "getdatadong: " + userLogin);

                        ref.child("Kelompok").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot kelompokSnapshot : dataSnapshot.getChildren()) {
                                    Kelompok kelompok = FormatManager.parseTo(Kelompok.class, new Gson().toJson(kelompokSnapshot.getValue()));
                                    kelompok.setKey(kelompokSnapshot.getKey());
                                    Log.e("TAG", "onDataChange: " + kelompok );
                                    Log.e("TAG", "onDataChange: " + kelompok.toString() );
                                    Log.e("TAG", "onDataChange: " + kelompok.getNama() );
                                    Log.e("TAG", "onDataChange: " + new Gson().toJson(kelompokSnapshot.getValue()) );
                                    if (kelompok != null){
                                        if (kelompok.getMentor().equals(userLogin)){
                                            kelompoks.add(kelompok);
                                            adapter.notifyDataSetChanged();
//                                            for (Kelompok item : kelompok){
//                                                datas.add(item);
//                                                paketSoalAdapter.notifyDataSetChanged();
//                                            }
                                        }
                                    }

//                                    String namaKelompok = kelompokSnapshot.child("nama").getValue(String.class);
//                                    String namaMentor = kelompokSnapshot.child("mentor").getValue(String.class);
//                                    if (namaMentor.equals(userLogin) && namaKelompok != null) {
//                                        kelompoks.add(namaKelompok + " , " + namaMentor);
//                                        Log.e("USER LOGIN", "namanya: " + userLogin);
//                                        Log.e("Kelompok", "onDataChange: " + kelompoks);
//                                        if (kelompoks.isEmpty()) {
//                                            finish();
//                                            PopupManager.showToast("Anda belum memiliki kelompok binaan");
//                                        }
//                                    }
                                }

                                dismissProgressBarDialog();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, DataKelompokActivity.class);
    }
}
