package shaf.com.optime.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.models.Kelompok;
import shaf.com.optime.models.Mentee;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;

public class InputMenteeActivity extends MasterActivity {

    Toolbar toolbar;
    EditText input_namaMentee, input_jurusan, input_angkatan, input_telepon, input_alamat, input_hobi, input_catatan;
    BetterSpinner input_gender, input_goldar, input_namaKelompok;
    Button submitMentee;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference("Mentee");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_mentee);

        toolbar = findViewById(R.id.toolbar);
        input_namaKelompok = findViewById(R.id.spinner_kelompok);
        input_namaMentee = findViewById(R.id.et_namaMentee);
        input_gender = findViewById(R.id.spinner_gender);
        input_jurusan = findViewById(R.id.et_jurusan);
        input_angkatan = findViewById(R.id.et_angkatan);
        input_telepon = findViewById(R.id.et_telepon);
        input_alamat = findViewById(R.id.et_alamat);
        input_goldar = findViewById(R.id.spinner_goldar);
        input_hobi = findViewById(R.id.et_hobi);
        input_catatan = findViewById(R.id.et_catatan);
        submitMentee = findViewById(R.id.btn_submitMentee);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Input Mentee");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showProgressBarDialog();
        getDataSpinner();
        String[] listGender = {"Ikhwan", "Akhwat"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listGender);
        input_gender.setAdapter(adapter);
        String[] listGoldar = {"O", "A", "B", "AB"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listGoldar);
        input_goldar.setAdapter(adapter2);

        final Mentee mentee = (Mentee) getIntent().getSerializableExtra("data");
        if (mentee != null) {
            input_namaKelompok.setText(mentee.getKelompok() + ", " + mentee.getMentor());
            input_namaMentee.setText(mentee.getNama());
            input_gender.setText(mentee.getGender());
            input_jurusan.setText(mentee.getJurusan());
            input_angkatan.setText(mentee.getAngkatan());
            input_telepon.setText(mentee.getTelepon());
            input_alamat.setText(mentee.getAlamat());
            input_goldar.setText(mentee.getGoldar());
            input_hobi.setText(mentee.getHobi());
            input_catatan.setText(mentee.getCatatan());

            submitMentee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProgressBarDialog();
                    String[] separated = input_namaKelompok.getText().toString().split(",");
                    separated[0] = separated[0].trim();
                    separated[1] = separated[1].trim();
                    mentee.setKelompok(separated[0]);
                    mentee.setMentor(separated[1]);
                    mentee.setNama(input_namaMentee.getText().toString());
                    mentee.setGender(input_gender.getText().toString());
                    mentee.setJurusan(input_jurusan.getText().toString());
                    mentee.setAngkatan(input_angkatan.getText().toString());
                    mentee.setTelepon(input_telepon.getText().toString());
                    mentee.setAlamat(input_alamat.getText().toString());
                    mentee.setGoldar(input_goldar.getText().toString());
                    mentee.setHobi(input_hobi.getText().toString());
                    mentee.setCatatan(input_catatan.getText().toString());
                    updateMentee(mentee);
                }
            });
        } else {
            submitMentee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProgressBarDialog();
                    String[] separated = input_namaKelompok.getText().toString().split(",");
                    separated[0] = separated[0].trim();
                    separated[1] = separated[1].trim();

                    if (!isEmpty(input_namaMentee.getText().toString()) && !isEmpty(input_gender.getText().toString()) && !isEmpty(input_jurusan.getText().toString()) && !isEmpty(input_angkatan.getText().toString()) && !isEmpty(input_telepon.getText().toString()) && !isEmpty(input_alamat.getText().toString()) && !isEmpty(input_goldar.getText().toString()) && !isEmpty(input_hobi.getText().toString()) && !isEmpty(input_catatan.getText().toString()) && !isEmpty(input_namaKelompok.getText().toString())) {
                        addMentee(new Mentee(input_angkatan.getText().toString(), input_gender.getText().toString(), input_jurusan.getText().toString(), separated[0], separated[1], input_namaMentee.getText().toString(), input_telepon.getText().toString(), input_alamat.getText().toString(), input_hobi.getText().toString(), input_goldar.getText().toString(), input_catatan.getText().toString()));
                    } else {
                        PopupManager.show("Peringatan", "Data tidak boleh kosong");
                    }
                }
            });
        }
    }

    private boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    private void getDataSpinner() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ref.child("Kelompok").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> kelompoks = new ArrayList<>();

                for (DataSnapshot kelompokSnapshot : dataSnapshot.getChildren()) {
                    String namaKelompok = kelompokSnapshot.child("nama").getValue(String.class);
                    String namaMentor = kelompokSnapshot.child("mentor").getValue(String.class);
                    if (namaMentor != null && namaKelompok != null) {
                        kelompoks.add(namaKelompok + ", " + namaMentor);
                        Log.e("Kelompok", "onDataChange: " + kelompoks);
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(InputMenteeActivity.this,
                        android.R.layout.simple_dropdown_item_1line, kelompoks);
                input_namaKelompok.setAdapter(adapter);
                dismissProgressBarDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateMentee(Mentee mentee) {
        ref.child(mentee.getKelompok()).child(mentee.getKey()).setValue(mentee).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                PopupManager.showToast("Data Berhasil Diupdate");
                finish();
                NavigationManager.openPage(DataMenteeActivity.class);
            }
        });
    }

    private void addMentee(Mentee mentee) {
        ref.child(mentee.getKelompok()).push().setValue(mentee, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    PopupManager.showToast("Gagal menambahkan mentee");
                    dismissProgressBarDialog();
                } else {
                    PopupManager.showToast("Berhasil menambahkan mentee");
                    dismissProgressBarDialog();
                    input_alamat.setText("");
                    input_angkatan.setText("");
                    input_catatan.setText("");
                    input_namaKelompok.setText("");
                    input_gender.setText("");
                    input_goldar.setText("");
                    input_hobi.setText("");
                    input_jurusan.setText("");
                    input_namaMentee.setText("");
                    input_telepon.setText("");
                    input_alamat.clearFocus();
                    input_telepon.clearFocus();
                    input_namaMentee.clearFocus();
                    input_jurusan.clearFocus();
                    input_catatan.clearFocus();
                    input_angkatan.clearFocus();
                    input_hobi.clearFocus();
                }
            }
        });
    }

    public static Intent getActIntent(Activity activity) {
        return new Intent(activity, InputMenteeActivity.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
