package shaf.com.optime.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.models.AbsenMentee;
import shaf.com.optime.services.AbsenManager;
import shaf.com.optime.utilities.AppLoader;
import shaf.com.optime.utilities.LayoutEmpty;

public class MasterActivity extends AppCompatActivity {
    public LayoutEmpty EMPTY;
    public AppLoader LOADER;
    public AbsenManager ABSEN;
    public void initEmpty(View view)
    {
        EMPTY = new LayoutEmpty(view);
    }
    public void initEmpty()
    {
        EMPTY = new LayoutEmpty();
    }
    protected void initLoader()
    {
        LOADER = new AppLoader(this);
    }
    protected void initActivity()
    {
        BaseApplication.ACTIVITY = this;
    }
    protected void initAbsen(){ ABSEN = new AbsenManager(); }

    private ProgressDialog mProgressBar;
    private boolean isLoading = false;

    @Override
    protected void onResume() {
        super.onResume();
        initActivity();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivity();
        initAbsen();
    }

    /** Dialog */
    public void showProgressBarDialog() {
      showProgressBarDialog(true);
    }

    public void showProgressBarDialogNotCancleAble() {
      showProgressBarDialog(false);
    }

    public void showProgressBarDialog(boolean isCancelAble) {
      if(mProgressBar == null) mProgressBar = new ProgressDialog(this);

      isLoading = true;

      if(mProgressBar != null) {
        mProgressBar.setCancelable(isCancelAble);
        mProgressBar.setMessage("Mohon tunggu ...");
        mProgressBar.show();
      }
    }

    public void dismissProgressBarDialog() {
      isLoading = false;

      if(mProgressBar != null)
        mProgressBar.dismiss();
    }
    /** [End] Dialog */

    public Uri getImageUri(Context inContext, Bitmap inImage) {
      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
      String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
      if (path != null)
          return Uri.parse(path);
      else
          return null;
    }

    public String getRealPathFromURI(Uri uri) {
      Cursor cursor = getContentResolver().query(uri, null, null, null, null);
      cursor.moveToFirst();
      int idx = cursor.getColumnIndex(Images.ImageColumns.DATA);
      return cursor.getString(idx);
    }

}
