package shaf.com.optime.utilities;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.interfaces.IEndlessScrollListener;
import shaf.com.optime.interfaces.IPullToRefreshListener;

public class ListProvider {
    public static void begin(RecyclerView recyclerView, RecyclerView.Adapter adapter, int column)
    {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(BaseApplication.ACTIVITY, column);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridDecorator(column,GridDecorator.dpToPx(2), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    public static EndlessScrollProvider getEndlessScrollProvider(RecyclerView recyclerView, final IEndlessScrollListener listener)
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BaseApplication.ACTIVITY);
        recyclerView.setLayoutManager(linearLayoutManager);
        return new EndlessScrollProvider(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                listener.onLoad(page,totalItemsCount);
            }
        };
    }

    public static void setEndlessScrolling(RecyclerView recyclerView,EndlessScrollProvider provider)
    {
        recyclerView.addOnScrollListener(provider);
    }

    public static <T> void setPullToRefresh(final SwipeRefreshLayout layoutRefresh, final IPullToRefreshListener listener)
    {
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                layoutRefresh.setRefreshing(true);
                listener.onRefresh();
            }
        });
    }
}
