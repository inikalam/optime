package shaf.com.optime.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.yarolegovich.lovelydialog.LovelyChoiceDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.List;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.fragments.DateTimeFragment;
import shaf.com.optime.interfaces.IConfirmationListener;
import shaf.com.optime.interfaces.IDatePickerListener;
import shaf.com.optime.interfaces.IPopupListener;

public class PopupManager {
    public static void createAnnouncement(String title , String message)
    {
        NotificationManager notificationManager = (NotificationManager) BaseApplication.ACTIVITY.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "announcement";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(BaseApplication.ACTIVITY, NOTIFICATION_CHANNEL_ID);

        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_notif_new)
                .setTicker("Hearty365")
                .setContentTitle(title)
                .setContentText(message)
                .setContentInfo("Info");

        notificationManager.notify((int) Math.random(), notificationBuilder.build());
    }

    public static void showToast(String message)
    {
        Toast.makeText(BaseApplication.ACTIVITY,message, Toast.LENGTH_LONG).show();
    }

    public static void showListDialog(String title, String message , List items, LovelyChoiceDialog.OnItemSelectedListener listener)
    {
        new LovelyChoiceDialog(BaseApplication.ACTIVITY)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setTitle(title)
                .setIcon(R.drawable.ic_logo_splash)
                .setMessage(message)
                .setItems(items, listener)
                .show();
    }

    public static void showWithAction(String title, String message, final IPopupListener listener)
    {
        new LovelyStandardDialog(BaseApplication.ACTIVITY, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.ic_logo_splash)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ya", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onOk();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public static void showNotDisposeable(String title, String message)
    {
        new LovelyStandardDialog(BaseApplication.ACTIVITY, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.ic_logo_splash)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ya", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        NavigationManager.openGooglePlay();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public static void show(String title, String message)
    {
        new LovelyStandardDialog(BaseApplication.ACTIVITY, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.ic_logo_splash)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ya", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                })
                .show();
    }

    public static void confirmation(String title, String message , final IConfirmationListener listener)
    {
        new LovelyStandardDialog(BaseApplication.ACTIVITY, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.ic_logo_splash)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ya", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onOk();
                    }
                })
                .setNegativeButton("Tidak", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onCancel();
                    }
                })
                .show();
    }
    public static void confirmationNotDisposeable(String title, String message , final IConfirmationListener listener)
    {
        new LovelyStandardDialog(BaseApplication.ACTIVITY)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorPrimary)
                .setIcon(R.drawable.logo_optime_head)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ya", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onOk();
                    }
                })
                .setNegativeButton("Tidak", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onCancel();
                    }
                })
                .setCancelable(false)
                .show();
    }

    public static void dateSelection(IDatePickerListener listener)
    {
        DateTimeFragment fragment = new DateTimeFragment();
        fragment.setListener(listener);
        fragment.show(BaseApplication.ACTIVITY.getSupportFragmentManager(), "datePicker");
    }
}
