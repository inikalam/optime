package shaf.com.optime.services;

import android.content.Context;
import android.net.ConnectivityManager;

import shaf.com.optime.BaseApplication;

public class NetworkManager {
    public static boolean hasInternetConnection() {

        ConnectivityManager con_manager = (ConnectivityManager)
                BaseApplication.ACTIVITY.getSystemService(Context.CONNECTIVITY_SERVICE);

        return (con_manager.getActiveNetworkInfo() != null
                && con_manager.getActiveNetworkInfo().isAvailable()
                && con_manager.getActiveNetworkInfo().isConnected());
    }
}
