package shaf.com.optime.services;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import shaf.com.optime.activities.MasterActivity;
import shaf.com.optime.interfaces.IAbsenListener;
import shaf.com.optime.models.Mentee;

public class AbsenManager {
    FirebaseDatabase database;
    DatabaseReference dbReference;
    FirebaseOptions options;
    String room = null;
    String subPath = "Mentee/";

    String TAG = MasterActivity.class.getSimpleName();

    public void initAbsen(final String kelompok, final IAbsenListener listener)
    {
        this.room = subPath + kelompok;
        database = FirebaseDatabase.getInstance();
        dbReference = database.getReference(room);

        dbReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                listener.absenAdded(FormatManager.parseTo(Mentee.class, new Gson().toJson(dataSnapshot.getValue())));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
