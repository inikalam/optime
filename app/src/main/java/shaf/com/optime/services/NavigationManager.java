package shaf.com.optime.services;

import android.content.Intent;
import android.net.Uri;

import shaf.com.optime.BaseApplication;

public class NavigationManager {

    public static void openLink(String link)
    {
        try {
            BaseApplication.ACTIVITY.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
        } catch (android.content.ActivityNotFoundException anfe) {
        }
    }
    public static void openPage( Class<?> destination)
    {
        Intent intent = new Intent(BaseApplication.ACTIVITY,destination);
        BaseApplication.ACTIVITY.startActivity(intent);
    }

    public static void openPageWithData( Class<?> destination, Intent intent)
    {
        intent.setClass(BaseApplication.ACTIVITY,destination);
        BaseApplication.ACTIVITY.startActivity(intent);
    }

    public static void openPageForResult(Class<?> destination, int requestCode){
      Intent intent = new Intent(BaseApplication.ACTIVITY, destination);
      BaseApplication.ACTIVITY.startActivityForResult(intent, requestCode);

    }

}
