package shaf.com.optime.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.hawk.Hawk;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.activities.DataKelompokActivity;
import shaf.com.optime.activities.InputKelompokActivity;
import shaf.com.optime.activities.InputLaporanActivity;
import shaf.com.optime.activities.InputMenteeActivity;
import shaf.com.optime.activities.MasterActivity;
import shaf.com.optime.adapters.SubMenuAdapter;
import shaf.com.optime.interfaces.ISubMenuListener;
import shaf.com.optime.models.SubMenu;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;
import shaf.com.optime.utilities.ListProvider;

public class DashboardFragment extends Fragment {
    private MasterActivity context;

    private RecyclerView recyclerView;
    private CarouselView carouselView;
    private List<SubMenu> menus;
    int[] sampleImages = {R.drawable.banner0, R.drawable.banner1, R.drawable.banner2, R.drawable.banner3, R.drawable.banner4, R.drawable.banner5, R.drawable.banner6};

    FirebaseAuth auth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseApplication.ACTIVITY;
        menus = new ArrayList<>();
        Hawk.init(getActivity()).build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        carouselView = view.findViewById(R.id.banner);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

        getBundle();

        return view;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    private void getBundle() {
        menus.clear();

        SubMenu dataKelompokMenu = new SubMenu();
        dataKelompokMenu.setCode("#1abc9c");
        dataKelompokMenu.setImgResId(R.drawable.ic_kelompok);
        dataKelompokMenu.setName("Data Kelompok");
        dataKelompokMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                NavigationManager.openPage(DataKelompokActivity.class);
                PopupManager.showToast("Data Kelompok sedang dalam pengerjaan");
            }
        });
        menus.add(dataKelompokMenu);

        SubMenu dataMenteeMenu = new SubMenu();
        dataMenteeMenu.setCode("#1abc9c");
        dataMenteeMenu.setImgResId(R.drawable.ic_mentee);
        dataMenteeMenu.setName("Data Mentee");
        dataMenteeMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
//                NavigationManager.openPage(InputMenteeActivity.class);
                PopupManager.showToast("Data Memtee sedang dalam pengerjaan");
            }
        });
        menus.add(dataMenteeMenu);

        SubMenu dataLaporanMenu = new SubMenu();
        dataLaporanMenu.setCode("#1abc9c");
        dataLaporanMenu.setImgResId(R.drawable.ic_laporan);
        dataLaporanMenu.setName("Data Laporan Halaqah");
        dataLaporanMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
//                NavigationManager.openPage(InputLaporanActivity.class);
                PopupManager.showToast("Data Laporan sedang dalam pengerjaan");
            }
        });
        menus.add(dataLaporanMenu);

        SubMenu inputKelompokMenu = new SubMenu();
        inputKelompokMenu.setCode("#1abc9c");
        inputKelompokMenu.setImgResId(R.drawable.ic_kelompok);
        inputKelompokMenu.setName("Input Data Kelompok");
        inputKelompokMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                NavigationManager.openPage(InputKelompokActivity.class);
            }
        });
        menus.add(inputKelompokMenu);

        SubMenu inputMenteeMenu = new SubMenu();
        inputMenteeMenu.setCode("#1abc9c");
        inputMenteeMenu.setImgResId(R.drawable.ic_mentee);
        inputMenteeMenu.setName("Input Data Mentee");
        inputMenteeMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                NavigationManager.openPage(InputMenteeActivity.class);
            }
        });
        menus.add(inputMenteeMenu);

        SubMenu inputLaporanMenu = new SubMenu();
        inputLaporanMenu.setCode("#1abc9c");
        inputLaporanMenu.setImgResId(R.drawable.ic_laporan);
        inputLaporanMenu.setName("Input Laporan Halaqah");
        inputLaporanMenu.setListener(new ISubMenuListener() {
            @Override
            public void onSelected() {
                NavigationManager.openPage(InputLaporanActivity.class);
            }
        });
        menus.add(inputLaporanMenu);

        SubMenuAdapter adapter = new SubMenuAdapter(menus);
        ListProvider.begin(recyclerView, adapter, 3);

        auth = FirebaseAuth.getInstance();
        Log.e("USER LOGIN", "onCreate: "+ auth.getCurrentUser().getEmail());

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference();

        ref.child("Mentor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot mentorSnapshot: dataSnapshot.getChildren()) {
                    String namaMentor = mentorSnapshot.child("fullname").getValue(String.class);
                    String emailMentor = mentorSnapshot.child("email").getValue(String.class);
                    if (emailMentor.equals(auth.getCurrentUser().getEmail())){
                        Hawk.put("fullname", namaMentor);
                        Hawk.put("email", emailMentor);
                        Hawk.put("gender", mentorSnapshot.child("gender").getValue(String.class));
                        Hawk.put("telepon", mentorSnapshot.child("telepon").getValue(String.class));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
