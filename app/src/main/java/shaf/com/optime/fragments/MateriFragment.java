package shaf.com.optime.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.activities.InputLaporanActivity;
import shaf.com.optime.activities.MasterActivity;
import shaf.com.optime.adapters.MateriAdapter;
import shaf.com.optime.models.Materi;
import shaf.com.optime.utilities.ListProvider;

public class MateriFragment extends Fragment {
    private MasterActivity context;

    private RecyclerView recyclerView;
    private MateriAdapter materiAdapter;
    private List<Materi> materis;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseApplication.ACTIVITY;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_materi, container, false);
        context.initEmpty(view);
        recyclerView = view.findViewById(R.id.recycler_view);

        context.showProgressBarDialog();

//        materis.clear();
        loadData();
        materis = new ArrayList<>();
        materiAdapter = new MateriAdapter(materis);
        ListProvider.begin(recyclerView, materiAdapter, 1);
        context.EMPTY.show(R.drawable.ic_post_it_128dp,"Belum ada materi","Saat ini belum tersedia materi untuk disampaikan");

        return view;
    }

    private void loadData() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

        ref.child("Materi").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot materiSnapshot : dataSnapshot.getChildren()) {
                    Materi materi = materiSnapshot.getValue(Materi.class);
                    materis.add(materi);
                }
                if (materis.size() == 0){
                    context.EMPTY.show(R.drawable.ic_post_it_128dp,"Belum ada materi","Saat ini belum tersedia materi untuk disampaikan");
                } else {
                    materiAdapter.notifyDataSetChanged();
                    context.EMPTY.dismiss();
                }
                context.dismissProgressBarDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
