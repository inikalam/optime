package shaf.com.optime.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.orhanobut.hawk.Hawk;

import androidx.fragment.app.Fragment;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.activities.LoginActivity;
import shaf.com.optime.activities.MasterActivity;
import shaf.com.optime.interfaces.IConfirmationListener;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;

public class ProfileFragment extends Fragment {

    MasterActivity context;
    LinearLayout buttonKeluar;
    TextView text_name, text_email, text_gender, text_telepon;

    FirebaseAuth auth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseApplication.ACTIVITY;
        Hawk.init(getActivity()).build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        auth = FirebaseAuth.getInstance();

        text_name = view.findViewById(R.id.text_name);
        text_name.setText(Hawk.get("fullname").toString());
        text_email = view.findViewById(R.id.text_email);
        text_email.setText(Hawk.get("email").toString());
        text_gender = view.findViewById(R.id.text_gender);
        text_gender.setText(Hawk.get("gender").toString());
        text_telepon = view.findViewById(R.id.text_telepon);
        text_telepon.setText(Hawk.get("telepon").toString());

        buttonKeluar = view.findViewById(R.id.btn_keluar);
        buttonKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupManager.confirmationNotDisposeable("Konfirmasi", "Apakah kamu yakin akan keluar dari aplikasi?", new IConfirmationListener() {
                    @Override
                    public void onOk() {
                        auth.signOut();
                        NavigationManager.openPage(LoginActivity.class);
                        context.finish();
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        });

        return view;
    }
}
