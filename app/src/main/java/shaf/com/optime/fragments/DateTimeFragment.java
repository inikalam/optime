package shaf.com.optime.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.interfaces.IDatePickerListener;

public class DateTimeFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private int month, year, day;
    private IDatePickerListener listener;

    public void setListener(IDatePickerListener listener) {
        this.listener = listener;
    }

    public DateTimeFragment() {
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.day = dayOfMonth;
        listener.setDateResult(year, month, day);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new DatePickerDialog(BaseApplication.ACTIVITY, this, year, month, day);
    }
}