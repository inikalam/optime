package shaf.com.optime.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.models.SubMenu;
import shaf.com.optime.viewholders.SubMenuViewHolder;

public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuViewHolder> {

    private List<SubMenu> data = new ArrayList<>();
    public SubMenuAdapter(List<SubMenu> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public SubMenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_menu_sub, viewGroup, false);
        return new SubMenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubMenuViewHolder menuViewHolder, int i) {
        final SubMenu item = data.get(i);

        menuViewHolder.getImageThumbnail().setImageDrawable(BaseApplication.ACTIVITY.getResources().getDrawable(item.getImgResId()));
        menuViewHolder.getTextTitle().setText(item.getName());
        menuViewHolder.getLayoutRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.getListener().onSelected();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}

