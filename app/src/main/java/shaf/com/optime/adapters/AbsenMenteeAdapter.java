package shaf.com.optime.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.activities.InputLaporanActivity;
import shaf.com.optime.models.AbsenMentee;
import shaf.com.optime.models.Mentee;
import shaf.com.optime.services.PopupManager;
import shaf.com.optime.viewholders.AbsenViewHolder;

public class AbsenMenteeAdapter extends RecyclerView.Adapter<AbsenViewHolder> {

    private List<Mentee> data;
    private Context context;

    public AbsenMenteeAdapter(List<Mentee> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public AbsenViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_absen_mentee, viewGroup, false);
        return new AbsenViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AbsenViewHolder absenViewHolder, int i) {
        final Mentee item = data.get(i);
        if (item != null) {
            absenViewHolder.getTxtNama().setText(item.getNama());
            absenViewHolder.getTxtKelas().setText(item.getJurusan());

            absenViewHolder.getBtnYes().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    PopupManager.showToast("Hadir");
                    absenViewHolder.getBtnYes().setVisibility(View.GONE);
                    absenViewHolder.getBtnNo().setVisibility(View.GONE);
                    if (context instanceof InputLaporanActivity) {
                        ((InputLaporanActivity) context).setStatus(item.getNama(), true);
                    }
                }
            });

            absenViewHolder.getBtnNo().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    PopupManager.showToast("Tidak Hadir");
                    absenViewHolder.getBtnNo().setVisibility(View.GONE);
                    absenViewHolder.getBtnYes().setVisibility(View.GONE);
                    if (context instanceof InputLaporanActivity) {
                        ((InputLaporanActivity) context).setStatus(item.getNama(), false);
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
