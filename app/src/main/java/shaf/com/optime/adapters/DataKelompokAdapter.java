package shaf.com.optime.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.activities.DataKelompokActivity;
import shaf.com.optime.activities.DataMenteeActivity;
import shaf.com.optime.activities.InputKelompokActivity;
import shaf.com.optime.interfaces.IConfirmationListener;
import shaf.com.optime.models.Kelompok;
import shaf.com.optime.services.NavigationManager;
import shaf.com.optime.services.PopupManager;

public class DataKelompokAdapter extends RecyclerView.Adapter<DataKelompokAdapter.DataKelompokViewHolder> {

    private List<Kelompok> data;
    private Context context;
    Intent intent = new Intent();

    public DataKelompokAdapter(List<Kelompok> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public DataKelompokViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_data_kelompok, viewGroup, false);
        return new DataKelompokViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataKelompokViewHolder holder, int i) {
        final Kelompok item = data.get(i);
        if (item != null){
            holder.txtKelompok.setText(item.getNama());
            holder.txtTingkat.setText("Tingkat: " + item.getTingkat());

            holder.layoutKelompok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    intent.putExtra("namaKelompok", item.getNama());
                    NavigationManager.openPageWithData(DataMenteeActivity.class, intent);
                }
            });
            holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupManager.showToast("Klik edit");
                    context.startActivity(InputKelompokActivity.getActIntent((Activity) context).putExtra("data", item));
                    ((Activity) context).finish();
                }
            });
            holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupManager.confirmationNotDisposeable("HAPUS DATA", "Hapus Data Kelompok Ini?", new IConfirmationListener() {
                        @Override
                        public void onOk() {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            Query applesQuery = ref.child("Kelompok").orderByChild("nama").equalTo(item.getNama());

                            applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                        appleSnapshot.getRef().removeValue();
                                        if (context instanceof DataKelompokActivity){
                                            ((DataKelompokActivity) context).afterUpdate();
                                        }
                                        holder.cardKelompok.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("TAG", "onCancelled", databaseError.toException());
                                }
                            });
                        }

                        @Override
                        public void onCancel() {
                            PopupManager.showToast("Dibatalkan");
                        }
                    });
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class DataKelompokViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout layoutKelompok, buttonEdit, buttonDelete;
        private TextView txtKelompok, txtTingkat;
        private CardView cardKelompok;

        public DataKelompokViewHolder(@NonNull View itemView) {
            super(itemView);
            this.layoutKelompok = itemView.findViewById(R.id.layoutKelompok);
            this.buttonEdit = itemView.findViewById(R.id.buttonEdit);
            this.buttonDelete = itemView.findViewById(R.id.buttonDelete);
            this.txtKelompok = itemView.findViewById(R.id.txtKelompok);
            this.txtTingkat = itemView.findViewById(R.id.txtTingkat);
            this.cardKelompok = itemView.findViewById(R.id.cardKelompok);
        }
    }
}
