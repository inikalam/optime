package shaf.com.optime.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.models.Menu;
import shaf.com.optime.services.MenuManager;
import shaf.com.optime.viewholders.MenuViewHolder;

public class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {

    private List<Menu> data = new ArrayList<>();
    public MenuAdapter(List<Menu> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_menu, viewGroup, false);
        return new MenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuViewHolder menuViewHolder, int i) {
        final Menu item = data.get(i);

        RequestOptions requestOptions = new RequestOptions();
        Glide.with(BaseApplication.ACTIVITY)
                .load(item.getImageUrl()).apply(requestOptions)
                .into(menuViewHolder.getImageThumbnail());
        /*menuViewHolder.getTextTitle().setText(item.getName());*/
        menuViewHolder.getLayoutRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuManager.open(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
