package shaf.com.optime.adapters;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import shaf.com.optime.BaseApplication;
import shaf.com.optime.R;
import shaf.com.optime.models.Materi;
import shaf.com.optime.services.PopupManager;
import shaf.com.optime.viewholders.MateriViewHolder;

public class MateriAdapter extends RecyclerView.Adapter<MateriViewHolder> {
    private List<Materi> data;

    public MateriAdapter(List<Materi> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public MateriViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_materi, viewGroup, false);
        return new MateriViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MateriViewHolder materiViewHolder, final int i) {
        final Materi item = data.get(i);
        if (item != null){
            materiViewHolder.getTextTitle().setText(item.getNama());
            materiViewHolder.getTextDeskripsi().setText(item.getDeskripsi());

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_image);
            requestOptions.error(R.drawable.no_image);
            Glide.with(BaseApplication.ACTIVITY)
                    .load(R.drawable.logo_optime_head).apply(requestOptions)
                    .into(materiViewHolder.getImageIcon());

            RequestOptions requestOptions2 = new RequestOptions();
            requestOptions2.placeholder(R.drawable.no_image);
            requestOptions2.error(R.drawable.no_image);
            Glide.with(BaseApplication.ACTIVITY)
                    .load(R.drawable.ic_download).apply(requestOptions2)
                    .into(materiViewHolder.getDownloadIcon());

            materiViewHolder.getLayoutRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    PopupManager.showToast(item.getFile());
                    Uri uri = Uri.parse(item.getFile()); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    BaseApplication.ACTIVITY.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
