package shaf.com.optime.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import shaf.com.optime.R;
import shaf.com.optime.activities.DataMenteeActivity;
import shaf.com.optime.activities.InputMenteeActivity;
import shaf.com.optime.interfaces.IConfirmationListener;
import shaf.com.optime.models.Mentee;
import shaf.com.optime.services.PopupManager;

public class DataMenteeAdapter extends RecyclerView.Adapter<DataMenteeAdapter.DataMenteeViewHolder> {

    private List<Mentee> data;
    private Context context;
    String namaKelompok;

    public DataMenteeAdapter(List<Mentee> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public DataMenteeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_data_mentee, viewGroup, false);
        return new DataMenteeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataMenteeViewHolder holder, int i) {
        if (context instanceof DataMenteeActivity){
            namaKelompok = ((DataMenteeActivity) context).getNamaKelompok();
        }
        final Mentee item = data.get(i);
        if (item != null){
            holder.txtNamaMentee.setText(item.getNama());
            holder.txtJurusan.setText(item.getJurusan());

            holder.layoutMentee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupManager.showToast("Klik layout mentee");
                }
            });
            holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupManager.showToast("Klik edit");
                    context.startActivity(InputMenteeActivity.getActIntent((Activity) context).putExtra("data", item));
                    ((Activity) context).finish();
                }
            });
            holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupManager.confirmationNotDisposeable("HAPUS DATA", "Hapus Data Mentee Ini?", new IConfirmationListener() {
                        @Override
                        public void onOk() {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            Query applesQuery = ref.child("Mentee").child(namaKelompok).orderByChild("nama").equalTo(item.getNama());

                            applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                        appleSnapshot.getRef().removeValue();
                                        if (context instanceof DataMenteeActivity){
                                            ((DataMenteeActivity) context).afterUpdate();
                                        }
                                        holder.cardMentee.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("TAG", "onCancelled", databaseError.toException());
                                }
                            });
                        }

                        @Override
                        public void onCancel() {
                            PopupManager.showToast("Dibatalkan");
                        }
                    });
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class DataMenteeViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout layoutMentee, buttonEdit, buttonDelete;
        private TextView txtNamaMentee, txtJurusan;
        private CardView cardMentee;

        public DataMenteeViewHolder(@NonNull View itemView) {
            super(itemView);
            this.layoutMentee = itemView.findViewById(R.id.layoutMentee);
            this.buttonEdit = itemView.findViewById(R.id.buttonEdit);
            this.buttonDelete = itemView.findViewById(R.id.buttonDelete);
            this.txtNamaMentee = itemView.findViewById(R.id.txtNamaMentee);
            this.txtJurusan = itemView.findViewById(R.id.txtJurusan);
            this.cardMentee = itemView.findViewById(R.id.cardMentee);
        }
    }
}
